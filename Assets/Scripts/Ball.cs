﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Ball : MonoBehaviour {

    public float speed = 30;
    private Rigidbody2D rigidBody;
    private AudioSource audioSource;

	// Use this for initialization
	void Start () {
        rigidBody = GetComponent<Rigidbody2D>();
        rigidBody.velocity = Vector2.right * speed;
	}

    private void OnCollisionEnter2D(Collision2D collision)
    {
        // PaddleLeft or PaddleRight
        if (collision.gameObject.name == "PaddleLeft" || collision.gameObject.name == "PaddleRight")
        {
            HandlePaddleHit(collision);    
        }
        
        // WallBottom or WallTop
        if (collision.gameObject.name == "WallBottom" || collision.gameObject.name == "WallTop")
        {
            SoundManager.Instance.PlayOneShot(SoundManager.Instance.wallBloop);
        }

        // GoalLeft or GoalRight
        if (collision.gameObject.name == "GoalLeft" || collision.gameObject.name == "GoalRight")
        {
            SoundManager.Instance.PlayOneShot(SoundManager.Instance.goalBloop);
            if (collision.gameObject.name == "GoalLeft")
            {
                IncreaseTextUIScore("ScoreUIRight");
            }
            else if (collision.gameObject.name == "GoalRight")
            {
                IncreaseTextUIScore("ScoreUILeft");
            }
            transform.position = new Vector2(0, 0);
        }
    }

    private void HandlePaddleHit(Collision2D collision)
    {
        float y = PaddleHitHeight(transform.position, collision.transform.position, collision.collider.bounds.size.y);
        Vector2 dir = new Vector2();
        if (collision.gameObject.name == "PaddleLeft")
        {
            dir = new Vector2(1, y).normalized;
        } else if (collision.gameObject.name == "PaddleRight")
        {
            dir = new Vector2(-1, y).normalized;
        }
        rigidBody.velocity = dir * speed;

        SoundManager.Instance.PlayOneShot(SoundManager.Instance.hitPaddleBloop);
    }

    private float PaddleHitHeight(Vector3 ball, Vector3 paddle, float paddleHeight)
    {
        return (ball.y - paddle.y) / paddleHeight;
    }

    private void UpdateTextUIScore(string textUIName, int newValue)
    {
        Text component = GameObject.Find(textUIName).GetComponent<Text>();
        UpdateTextUIScore(component, newValue);
    }

    private void UpdateTextUIScore(Text textUIComponent, int newValue)
    {
        textUIComponent.text = newValue.ToString();
    }

    private void IncreaseTextUIScore(string textUIName)
    {
        Text component = GameObject.Find(textUIName).GetComponent<Text>();
        UpdateTextUIScore(component, int.Parse(component.text) + 1);
    }
}
