﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SplashScreen : MonoBehaviour {

    public string nextScene;
    public int secondsToLoad;

	// Use this for initialization
	void Start () {
        Invoke("OpenNextScene", secondsToLoad);	
	}

    void OpenNextScene()
    {
        SceneManager.LoadScene(nextScene);
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
